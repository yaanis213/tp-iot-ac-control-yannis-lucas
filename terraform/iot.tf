# aws_iot_certificate cert

resource "aws_iot_certificate" "yltp-cert" {
  active = true
}


# aws_iot_policy pub-sub
resource "aws_iot_policy" "yltp-pubsub" {
  name = "PubSubToAnyTopic"

  policy = file("files/iot_policy.json")
}
# aws_iot_policy_attachment attachment

resource "aws_iot_policy_attachment" "yltp-attachment" {
  policy = aws_iot_policy.yltp-pubsub.name
  target = aws_iot_certificate.yltp-cert.arn
}

# aws_iot_thing temp_sensor
resource "aws_iot_thing" "yltp-temp-sensor" {
  name = "temperature-sensor"

  attributes = {
    First = "examplevalue"
  }
}

# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "yltp-thing_attachment" {
  principal = aws_iot_certificate.yltp-cert.arn
  thing     = aws_iot_thing.yltp-temp-sensor.name
}

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "yltp-endpoint" {
    endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "yltp-iot-topic-rule-1" {
  name        = "1"
  description = "Get temperatures superior to 40 °C"
  enabled     = true
  sql         = "SELECT *  FROM 'sensor/temperature/+' where temperature >= 40"
  sql_version = "2016-03-23"

    dynamodbv2 { 
      put_item {
        table_name = "Temperature"
      }

      role_arn = aws_iam_role.yltp-iot_role.arn
    }
}


# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "yltp-timestream" {
  name        = "2"
  description = "Get all datas"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

   timestream {
    database_name = "yltp-iot"
    table_name  = "yltp-temperaturesensor"
   dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }

    role_arn = aws_iam_role.yltp-iot_role.arn
    
   }
}

###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
