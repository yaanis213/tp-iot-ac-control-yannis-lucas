# aws_lambda_function to control air conditioner

resource "aws_lambda_function" "yltp-lambda" {
  # If the file is not in the current working directory you will need to include a 
  # path.module in the filename.
  filename      = "../ac_control_lambda/lambda_function_payload.zip"
  function_name = "ac_control_lambda"
  role          = aws_iam_role.yltp-lambda-role.arn
  runtime = "python3.7"
  handler = "ac_control_lambda.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  # source_code_hash = filebase64sha256("lambda_function_payload.zip")

}

# aws_cloudwatch_event_rule scheduled action every minute

resource "aws_cloudwatch_event_rule" "yltp-cloudwatch-event-rule" {
  name        = "yltp-cloudwatch"
  description = "Capture each second"
  schedule_expression = "rate(1 minute)"

  event_pattern = <<EOF
{
  "detail-type": [
    "AWS Console Sign In via CloudTrail"
  ]
}
EOF
}

# aws_cloudwatch_event_target to link the schedule event and the lambda function

resource "aws_cloudwatch_event_target" "yltp-target" {
  target_id = "lambda"
  rule      = aws_cloudwatch_event_rule.yltp-cloudwatch-event-rule.name
  arn       = aws_lambda_function.yltp-lambda.arn
}


# aws_lambda_permission to allow CloudWatch (event) to call the lambda function

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.yltp-lambda.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.yltp-cloudwatch-event-rule.arn
}