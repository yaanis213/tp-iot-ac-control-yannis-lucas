# aws_timestreamwrite_database

resource "aws_timestreamwrite_database" "yltp-timestream" {
  database_name = "yltp-iot"
}

resource "aws_timestreamwrite_table" "yltp-timestream-table" {
  database_name = aws_timestreamwrite_database.yltp-timestream.database_name
  table_name    = "yltp-temperaturesensor"
}

# aws_timestreamwrite_table linked to the database
