# aws_iam_role iot_role
resource "aws_iam_role" "yltp-iot_role" {
  name = "yltp-iot_role"

  assume_role_policy = jsonencode({
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "iot.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
})
}

# aws_iam_role_policy iam_policy_for_dynamodb
resource "aws_iam_role_policy" "yltp-db-policy" {
  name = "yltp-db-policy"
  role = aws_iam_role.yltp-iot_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "dynamodb:PutItem",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

# aws_iam_role_policy iam_policy_for_timestream_writing
resource "aws_iam_role_policy" "yltp-write-records" {
  name = "yltp-write-records"
  role = aws_iam_role.yltp-iot_role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "timestream:WriteRecords",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "timestream:DescribeEndpoints",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

# aws_iam_role lambda_role

resource "aws_iam_role" "yltp-lambda-role" {
  name = "yltp-lambda-role"

  assume_role_policy = jsonencode({
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
})
}


# aws_iam_role_policy iam_policy_for_timestream_reading for Lambda

resource "aws_iam_role_policy" "yltp-select-records" {
  name = "yltp-select-records"
  role = aws_iam_role.yltp-lambda-role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "timestream:Select",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action = [
          "timestream:DescribeEndpoints",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}


# aws_iam_role_policy iam_policy_for_iot_publishing for Lambda

resource "aws_iam_role_policy" "yltp-publish" {
  name = "yltp-publish"
  role = aws_iam_role.yltp-lambda-role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iot:Publish",
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}



###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

resource "aws_iam_role_policy" "iam_policy_for_logs" {
 name = "cloudwatch_policy"
 role = aws_iam_role.yltp-iot_role.id

 policy = <<EOF
{
       "Version": "2012-10-17",
       "Statement": [
           {
               "Effect": "Allow",
               "Action": [
                   "logs:CreateLogGroup",
                   "logs:CreateLogStream",
                   "logs:PutLogEvents",
                   "logs:PutMetricFilter",
                   "logs:PutRetentionPolicy"
                ],
               "Resource": [
                   "*"
               ]
           }
       ]
   }
EOF
}


###########################################################################################
# Enable the following resources to enable logging for your Lambda function (helps debug)
###########################################################################################

resource "aws_cloudwatch_log_group" "example" {
 name              = "/aws/lambda/${aws_lambda_function.yltp-lambda.function_name}"
 retention_in_days = 14
}

resource "aws_iam_policy" "lambda_logging" {
 name        = "lambda_logging"
 path        = "/"
 description = "IAM policy for logging from a lambda"

 policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": [
       "logs:CreateLogGroup",
       "logs:CreateLogStream",
       "logs:PutLogEvents"
     ],
     "Resource": "arn:aws:logs:*:*:*",
     "Effect": "Allow"
   }
 ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
 role       = aws_iam_role.yltp-lambda-role.name
 policy_arn = aws_iam_policy.lambda_logging.arn
}
